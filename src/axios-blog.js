import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://burger-teamplate-ramilya.firebaseio.com/'
});

export default instance;