import React, {Component} from 'react';
import axios from '../../axios-blog';
import Form from "../../components/Form/Form";

import './Add.css';
import Container from "../../components/Container/Container";

class Add extends Component {
  state = {
    title: '',
    description: ''
  };

  changeValueHandler = (event) => {
    const thisName = event.target.name;
    this.setState({[thisName]: event.target.value});
  };

  getTwoZeros = (date) => {
    if (date < 10) date = '0' + date;
    return date;
  };

  sendPost = (event) => {
    event.preventDefault();
    if (this.state.title && this.state.description) {
      const currentdate = new Date();
      const date = "Created on: " + currentdate.getUTCDate() + "/" + currentdate.getMonth() + 1
          + "/" + currentdate.getFullYear() + " "
          + this.getTwoZeros(currentdate.getHours()) + ":"
          + this.getTwoZeros(currentdate.getMinutes()) + ":" + this.getTwoZeros(currentdate.getSeconds());

      const message = {...this.state};
      message.date = date;

      axios.post('/blog.json', message).finally(() => {
        this.setState({title: '', description: ''});
      });
    } else {
      alert('Fields are empty!');
    }
  };

  render() {
    return (
        <div className="Add">
          <Container>
            <Form
                formTitle="Add new post"
                changeValue={event => this.changeValueHandler(event)}
                button="Send" sendPost={(event) => this.sendPost(event)}
                title={this.state.title}
                body={this.state.description}
            />
          </Container>
        </div>
    );
  }
}

export default Add;