import React, {Component} from 'react';
import Container from "../../components/Container/Container";
import Form from "../../components/Form/Form";
import axios from '../../axios-blog';

class Edit extends Component {
  state = {
    title: '',
    description: '',
    id: '',
    date: ''
  };

  componentDidMount() {
    const id = this.props.match.params.id;

    axios.get('/blog/' + id + '.json').then(post => {
      this.setState({
        title: post.data.title,
        description: post.data.description,
        date: post.data.date,
        id
      });
    })
  }

  changeValueHandler = (event) => {
    const thisName = event.target.name;
    this.setState({[thisName]: event.target.value});
  };

  saveChangesHandler = (event) => {
    event.preventDefault();
    axios.put('/blog/' + this.state.id + '.json', this.state).then(() => {
      this.props.history.replace('/');
    })
  };

  render() {
    return (
        <div className="Edit">
          <Container>
            <Form
                formTitle="Edit post"
                changeValue={event => this.changeValueHandler(event)}
                button="Save changes" sendPost={(event) => this.saveChangesHandler(event)}
                title={this.state.title}
                body={this.state.description}
            />
          </Container>
        </div>
    );
  }
}

export default Edit;