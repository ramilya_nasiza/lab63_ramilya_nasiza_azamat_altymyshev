import React, {Component, Fragment} from 'react';
import Container from "../../components/Container/Container";
import pic from './friends.jpg';

import './About.css';
class About extends Component {
  render() {
    return (
        <Fragment>
          <Container>
            <h3 className='AboutTitle'>About Us</h3>
            <p className='AboutBody'>Hello everybody! It's our lab work. We have been hardly working on it very long time! So we hope to get a good marks!
              Thanks :)</p>
            <img src={pic} alt="" className='AboutPic'/>
            <p className='AboutAuthor'>Ramilya & Azamat</p>
          </Container>
        </Fragment>
    );
  }
}

export default About;