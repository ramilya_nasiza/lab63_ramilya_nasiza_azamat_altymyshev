import React, {Component} from 'react';

import './Contact.css';

class Contact extends Component {
  render() {
    return (
        <div className='Contact'>
          Contact Us:
          <p>Telegram: milya__n && azabraza</p>
          <p>Email: ramilya@sunrisestudio.pro </p>
          <p>Thanks :)</p>
        </div>
    );
  }
}

export default Contact;