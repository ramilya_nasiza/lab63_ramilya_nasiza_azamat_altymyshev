import React, {Component} from 'react';
import axios from '../../axios-blog';

import './Home.css';
import Post from "../../components/Post/Post";
import Container from "../../components/Container/Container";

class Home extends Component {
  state = {
    posts: null
  };

  componentDidMount() {
    axios.get('/blog.json').then((response) => {
      const posts = Object.values(response.data);
      const keys = Object.keys(response.data);

      for (let key in posts) {
        posts[key].id = keys[key];
      }

      this.setState({posts});
    })
  }

  render() {

    let posts = () => {
      if (this.state.posts) {
        return this.state.posts.map((post) => (
            <Post
                date={post.date}
                body={post.description}
                key={post.id}
                href={'/posts/' + post.id}
            />
        ))
      } else {
        return (<div>No posts here...</div>)
      }
    };

    return (
        <div className="Home">
          <Container>
            {posts()}
          </Container>
        </div>
    );
  }
}

export default Home;