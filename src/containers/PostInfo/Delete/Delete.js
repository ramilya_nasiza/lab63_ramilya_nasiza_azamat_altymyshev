import React from 'react';

import './Delete.css';
const Delete = props => {
  return (
      <button className='Delete' onClick={props.delete}>Delete</button>
  );
};

export default Delete;